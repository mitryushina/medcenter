create table [dbo].[COURSE_COMPLEXITIES] (
  [id] bigint identity (1, 1) not null,
  [title] nvarchar(1024) not null,

  constraint [PK_COURSE_COMPLEXITY] primary key clustered ([id] asc)
)
go

create table [dbo].[COURSES] (
  [id] bigint identity (1, 1) not null,
  [code] nvarchar(255) not null,
  [title] nvarchar(1024) not null,
  [complexity_id] bigint not null,

  constraint [PK_COURSE] primary key clustered ([id] asc)
)
go

alter table [dbo].[COURSES] add constraint [FK_COURSE_COURSE_COMPLEXITY]
  foreign key ([complexity_id]) references [dbo].[COURSE_COMPLEXITIES] ([id])
go
