set identity_insert [dbo].[COURSE_COMPLEXITIES] on

insert into [dbo].[COURSE_COMPLEXITIES] ([id], [title])
  values (1, 'Basic');

insert into [dbo].[COURSE_COMPLEXITIES] ([id], [title])
  values (2, 'Medium');

insert into [dbo].[COURSE_COMPLEXITIES] ([id], [title])
  values (3, 'Complex');

insert into [dbo].[COURSE_COMPLEXITIES] ([id], [title])
  values (4, 'Professional');

set identity_insert [dbo].[COURSE_COMPLEXITIES] OFF
go

set identity_insert [dbo].[COURSES] on

insert into [dbo].[COURSES] ([id], [code], [title], [complexity_id])
  values (1, 'ALG', 'Algorithms', 2);
insert into [dbo].[COURSES] ([id], [code], [title], [complexity_id])
  values (2, 'OOP', 'Object-Oriented Programming', 2);
insert into [dbo].[COURSES] ([id], [code], [title], [complexity_id])
  values (3, 'ML', 'Machine Learning', 3);
insert into [dbo].[COURSES] ([id], [code], [title], [complexity_id])
  values (4, 'PROG', 'Programming Basics', 1);

set identity_insert [dbo].[COURSES] off