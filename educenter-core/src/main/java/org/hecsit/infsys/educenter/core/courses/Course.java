package org.hecsit.infsys.educenter.core.courses;

import org.hecsit.infsys.educenter.core.BaseEntity;

import javax.persistence.*;

@Entity
@Table(name = "COURSES")
public class Course extends BaseEntity {

    @Column(name = "code", length = 255, nullable = false)
    private String code;

    @Column(name = "title", length = 1024, nullable = false)
    private String title;

    @ManyToOne
    @JoinColumn(name = "complexity_id", nullable = false)
    private CourseComplexity complexity;

    protected Course() {}

    public Course(String code, String title, CourseComplexity complexity) {
        this.code = code;
        this.title = title;
        this.complexity = complexity;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public CourseComplexity getComplexity() {
        return complexity;
    }

    public void setComplexity(CourseComplexity complexity) {
        this.complexity = complexity;
    }
}
