package org.hecsit.infsys.educenter.core.model;

/**
 * Created by Наталья on 01.03.2016.
 */
public class Drug extends Entity {
    private String _name;
    private Record _record;
    private String _description;

    public Drug(String name, Record record, String description){
        _name = name;
        _record = record;
        _description = description;
    }

    public String GetName(){
        return _name;
    }

    public void SetName(String name){
        _name = name;
    }

    public Record GetRecord(){
        return _record;
    }

    public void SetRecord(Record record){
        _record = record;
    }

    public  String GetDescription(){
        return _description;
    }

    public void SetDescription(String description){
        _description = description;
    }
}
