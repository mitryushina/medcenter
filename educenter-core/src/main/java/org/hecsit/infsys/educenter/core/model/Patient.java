package org.hecsit.infsys.educenter.core.model;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Наталья on 01.03.2016.
 */
public class Patient extends Entity {
    private String _fullName;
    private Date _birthDate;
    private Passport _passport;
    private Policy _policy;
    private Contact _contact;

    public  Patient(String fullName, Date birthDate, Passport passport, Policy policy, Contact contact){
        _fullName = fullName;
        _birthDate = birthDate;
        _passport = passport;
        _policy = policy;
        _contact = contact;
    }

    public String GetFullName(){
        return  _fullName;
    }

    public void  SetFullName(String fullName){
        _fullName = fullName;
    }

    public Date GetBirthDate(){
        return _birthDate;
    }

    public void SetBirthDate(Date birthDate){
        _birthDate = birthDate;
    }

    public Passport GetPassport(){
        return _passport;
    }

    public void SetPassport(Passport passport){
        _passport = passport;
    }

    public Policy GetPolicy(){
        return _policy;
    }

    public void SetPolicy(Policy policy){
        _policy = policy;
    }

    public  Contact GetContact(){
        return _contact;
    }

    public void SetContact(Contact contact){
        _contact = contact;
    }

}
