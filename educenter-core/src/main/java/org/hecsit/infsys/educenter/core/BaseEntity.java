package org.hecsit.infsys.educenter.core;

import javax.persistence.*;

@MappedSuperclass
@Access(AccessType.FIELD)
public abstract class BaseEntity {

    private long id;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    @Access(AccessType.PROPERTY)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
