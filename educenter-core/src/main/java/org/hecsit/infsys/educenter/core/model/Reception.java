package org.hecsit.infsys.educenter.core.model;

import java.util.Date;

/**
 * Created by Наталья on 01.03.2016.
 */
public class Reception extends Entity {
    private Patient _patient;
    private Date _date;

    public Reception(Patient patient, Date date){
        _patient = patient;
        _date = date;
    }

    public Patient GetPatient(){
        return _patient;
    }

    public void SetPatient(Patient patient){
        _patient = patient;
    }

    public Date GetDate(){
        return _date;
    }

    public void SetDate(){
        _date = _date;
    }
}
