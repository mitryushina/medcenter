package org.hecsit.infsys.educenter.core.model;

/**
 * Created by Наталья on 01.03.2016.
 */
public class HealthProfessional extends Entity {
    private String _fullName;
    private  String _post;

    public HealthProfessional(String fullName, String post){
        _fullName = fullName;
        _post = post;
    }

    public String GetFullName(){
        return _fullName;
    }

    public void SetFullName(String fullName){
        _fullName = fullName;
    }

    public String GetPost(){
        return _post;
    }

    public void SetPost(String post){
        _post = post;
    }
}
