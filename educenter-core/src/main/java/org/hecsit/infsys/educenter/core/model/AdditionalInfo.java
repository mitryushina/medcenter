package org.hecsit.infsys.educenter.core.model;

/**
 * Created by Наталья on 01.03.2016.
 */
public class AdditionalInfo extends Entity {
    public enum AdditionalInfoType{
        BloodGroup,
        AllergicResponse,
        Intolerance
    }

    private Patient _patient;
    private AdditionalInfoType _type;
    private String _info;

    public AdditionalInfo(Patient patient, AdditionalInfoType type, String info){
       _patient = patient;
        _type = type;
        _info = info;
    }

    public Patient GetPatient(){ return _patient; }
    public  void SetPatient(Patient patient){
        _patient = patient;
    }
    public AdditionalInfoType GetType(){
        return _type;
    }

    public void SetType(AdditionalInfoType type){
        _type = type;
    }

    public String GetInfo(){
        return _info;
    }

    public  void SetInfo(String info){
        _info = info;
    }

}
