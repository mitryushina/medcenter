package org.hecsit.infsys.educenter.core.model;

import java.util.ArrayList;

/**
 * Created by Наталья on 01.03.2016.
 */
public class Record extends Entity {

    public enum RecordType{
        Consultation,
        Exploration
    }

    private RecordType _type;
    private String  _writing;
    private Patient _patient;
    private Diagnosis _diagnosis;
    private Reception _reception;
    private ArrayList<Enclosure> _enclosures;

    public Record(RecordType type, String writing, Patient patient, Diagnosis diagnosis, Reception reception, Enclosure enclosure){
        _type = type;
        _writing = writing;
        _patient = patient;
        _diagnosis = diagnosis;
        _reception = reception;
        _enclosures = new ArrayList<Enclosure>();
        _enclosures.add(enclosure);
    }

    public RecordType GetType(){
        return _type;
    }

    public void SetType(RecordType type){
        _type = type;
    }

    public String GetWriting(){
        return _writing;
    }

    public void SetWriting(String writing){
        _writing = writing;
    }

    public Patient GetPatient(){
        return _patient;
    }

    public void SetPatient(Patient patient){
        _patient = patient;
    }

    public Diagnosis GetDiagnosis(){
        return _diagnosis;
    }

    public void SetDiagnosis(Diagnosis diagnosis){
        _diagnosis = diagnosis;
    }

    public Reception GetReception(){
        return _reception;
    }

    public void SetReception(Reception reception){
        _reception = reception;
    }

    public ArrayList<Enclosure> GetEnclosures(){
        return _enclosures;
    }

    public void SetEnclosures(ArrayList<Enclosure> enclosures){
        _enclosures = enclosures;
    }

}
