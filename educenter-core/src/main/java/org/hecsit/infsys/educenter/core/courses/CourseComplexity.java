package org.hecsit.infsys.educenter.core.courses;

import org.hecsit.infsys.educenter.core.BaseEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "COURSE_COMPLEXITIES")
public class CourseComplexity extends BaseEntity {

    @Column(name = "title", length = 1024, nullable = false)
    private String title;

    protected CourseComplexity() {}

    public CourseComplexity(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
