package org.hecsit.infsys.educenter.core.model;

/**
 * Created by Наталья on 01.03.2016.
 */
public class Diagnosis extends Entity {
    public enum DiagnosisType{
        Рreliminary,
        Final
    }

    private String _name;
    private String _description;
    private DiagnosisType _type;
    private Patient _patient;

    public Diagnosis(String name, String description, DiagnosisType type, Patient patient){
        _name = name;
        _description = description;
        _type = type;
        _patient = patient;
    }

    public String GetName(){
        return  _name;
    }

    public void SetName(String name){
        _name = name;
    }

    public String GetDescription(){
        return _description;
    }

    public void SetDescription(String description){
        _description = description;
    }

    public  DiagnosisType GetType(){
        return  _type;
    }

    public void SetType(DiagnosisType type){
        _type = type;
    }

    public Patient GetPatient(){
        return _patient;
    }

    public void SetPatient(Patient patient){
        _patient = patient;
    }

}
