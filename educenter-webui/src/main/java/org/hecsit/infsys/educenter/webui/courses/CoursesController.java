package org.hecsit.infsys.educenter.webui.courses;

import org.hecsit.infsys.educenter.core.courses.Course;
import org.hecsit.infsys.educenter.core.courses.CourseComplexity;
import org.hecsit.infsys.educenter.core.courses.CoursesApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.text.MessageFormat;
import java.util.List;

@Controller
@RequestMapping("courses")
public class CoursesController {

    private final CoursesApi coursesApi;

    @Autowired
    public CoursesController(CoursesApi coursesApi) {
        this.coursesApi = coursesApi;
    }

    @RequestMapping(method = RequestMethod.GET)
    public String list(ModelMap model) {
        List<Course> courses = coursesApi.getCourses();
        model.addAttribute(courses);
        return "courses/list";
    }

    @RequestMapping(value = "/new", method = RequestMethod.GET)
    public String create(ModelMap model) {
        model.addAttribute("form", new CourseForm());
        return "courses/create";
    }

    @RequestMapping(value = "/new", method = RequestMethod.POST)
    public String create(@Valid @ModelAttribute("form") CourseForm form,
                         BindingResult result, final RedirectAttributes redirectAttributes) {

        if (result.hasErrors()) {
            return "courses/create";
        }
        coursesApi.createCourse(form.getCode(), form.getTitle(), form.getComplexityId());
        redirectAttributes.addFlashAttribute(
                "message", MessageFormat.format("Course {0} was added.", form.getCode()));
        return "redirect:/courses";
    }

    @ModelAttribute("complexityList")
    public List<CourseComplexity> addComplexities() {
        return coursesApi.getCourseComplexities();
    }
}
