<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>

<html>
<head>
    <title><sitemesh:write property='title'/></title>
    <sitemesh:write property='head'/>

    <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/main.css">
</head>

<s:url value="/courses" var="coursesListUrl" />

<body>
    <h1>EDUCENTER</h1>
    [<a href="${coursesListUrl}">Courses List</a>|Complexities List]
    <sitemesh:write property='body'/>
</body>
</html>