<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="f" uri="http://www.springframework.org/tags/form"%>

<jsp:useBean id="courseList" scope="request" type="java.util.List"/>

<s:url value="/courses/new" var="createCourseUrl" />

<html>
<head>
    <title>Courses List</title>
</head>
<body>
<h3>Courses List [<a href="${createCourseUrl}">Add New</a> ]</h3>

<c:if test="${not empty message}">
    <p class="message">${message}</p>
</c:if>

<table>
    <tr><td>Code</td><td>Title</td><td>Complexity</td></tr>
    <c:if test="${empty courseList}">
        <tr><td colspan="3">No Courses</td></tr>
    </c:if>
    <c:if test="${not empty courseList}">
        <c:forEach var="item" items="${courseList}">
            <tr><td>${item.code}</td><td>${item.title}</td><td>${item.complexity.title}</td></tr>
        </c:forEach>
    </c:if>
</table>

</body>
</html>
