<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<jsp:useBean id="complexityList" scope="request" type="java.util.List"/>

<s:url value="/courses/new" var="createCourseUrl" />

<html>
<head>
    <title>New Course</title>
</head>
<body>
<h3>New Course</h3>

<form:form method="post" modelAttribute="form" action="${createCourseUrl}">

    <label>Code:</label><form:input path="code" type="text"  /><form:errors path="code" cssClass="error" />
    <label>Title:</label><form:input path="title" type="text" /><form:errors path="title" cssClass="error" />
    <label>Complexity:</label><form:select path="complexityId" items="${complexityList}" itemLabel="title" itemValue="id" />
    <form:errors path="complexityId" cssClass="error" />
    <button type="submit">Create</button>
</form:form>

</body>
</html>
